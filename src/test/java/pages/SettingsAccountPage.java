package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SettingsAccountPage extends AbstractPage{

    @FindBy(xpath = "//input[@id='user_old_password']")
    private WebElement editTextOldPassword;

    @FindBy(xpath = "//input[@data-autocheck-url='/signup_check/password']")
    private WebElement editTextNewPassword;

    @FindBy(xpath = "//input[@id='user_confirm_new_password']")
    private WebElement editTextConfirmPassword;

    @FindBy(xpath = "//button[@class='btn']")
    private WebElement buttonUpdatePassword;

    @FindBy(xpath = "//div[contains(@class,'container')]")
    private WebElement labelStatus;

    public SettingsAccountPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    @Override
    public void openPage() {
        driver.navigate().to("https://github.com/settings/admin");
    }


    public String getLabelStatus() {
        return labelStatus.getText();
    }

    public WebElement getEditTextOldPassword() {
        return editTextOldPassword;
    }

    public void setEditTextOldPassword(String editTextOldPassword) {
        this.editTextOldPassword.sendKeys(editTextOldPassword);
    }

    public String getEditTextNewPassword() {
        return editTextNewPassword.getText();
    }

    public void setEditTextNewPassword(String editTextNewPassword) {
        this.editTextNewPassword.sendKeys(editTextNewPassword);
    }

    public String getEditTextConfirmPassword() {
        return editTextConfirmPassword.getText();
    }

    public void setEditTextConfirmPassword(String editTextConfirmPassword) {
        this.editTextConfirmPassword.sendKeys(editTextConfirmPassword);
    }

    public void buttonUpdatePasswordClick() {
        buttonUpdatePassword.click();
    }

}
