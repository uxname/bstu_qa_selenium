package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SettingsPage extends AbstractPage{

    @FindBy(xpath = "//input[@id='user_profile_name']")
    private WebElement editTextName;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement buttonUpdateProfile;

    @FindBy(xpath = "//div[@class='container']")
    private WebElement labelStatus;

    public SettingsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    @Override
    public void openPage() {
        driver.navigate().to("https://github.com/settings/profile");
    }

    public void clickUpdateProfile() {
        buttonUpdateProfile.click();
    }

    public void setEditTextName(String textName) {
        editTextName.clear();
        editTextName.sendKeys(textName);
    }

    public String getEditTextName() {
        return editTextName.getText();
    }

    public String getLabelStatus() {
        return labelStatus.getText();
    }

}
