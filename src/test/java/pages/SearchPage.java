package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Scanner;

public class SearchPage extends AbstractPage{

    @FindBy(xpath = "//*[@id=\"search_form\"]/div[2]/div[1]/input[1]")
    private WebElement editTextFindRepo;

    @FindBy(xpath = "//*[@id=\"search_form\"]/div[2]/div[2]/button")
    private WebElement buttonSearch;

    @FindBy(css = "#js-pjax-container > div.container > div > div.column.three-fourths.codesearch-results > div.sort-bar > h3")
    private WebElement textViewFound;

    public SearchPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    @Override
    public void openPage() {
        driver.navigate().to("https://github.com/search");
    }


    public void searchRepo(String reponame) {
        editTextFindRepo.sendKeys(reponame);
        buttonSearch.click();
    }

    public int getFoundRepoCount() {
        int res = 0;
        for (int i=0; i < textViewFound.getText().length(); i++) {
            char c = textViewFound.getText().charAt(i);
            if (c < '0' || c > '9') continue;
            res = res * 10 + (c - '0');
        }
        return res;
    }
}
