package pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GistPage extends AbstractPage{

    @FindBy(css = ".ace_text-input")
    private WebElement editTextGistText;

    @FindBy(css = ".btn-secret")
    private WebElement buttonCreateGistPrivate;

    @FindBy(css = "button.js-gist-create:nth-child(1)")
    private WebElement buttonCreateGistPublic;

    @FindBy(css = "#file-gistfile1-txt-LC1")
    private WebElement textViewGist;

    public GistPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(this.driver, this);
    }

    @Override
    public void openPage() {
        driver.navigate().to("https://gist.github.com/");
    }


    public void createGistPrivate(String text) {
        editTextGistText.sendKeys(text);
        buttonCreateGistPrivate.click();
    }

    public void createGistPublic(String text) {
        editTextGistText.sendKeys(text);
        buttonCreateGistPublic.click();
    }

    public String getCreatedGistText() {
        return textViewGist.getText();
    }
}
