package steps;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.log4testng.Logger;
import pages.*;

public class Steps
{
    private WebDriver driver;

    private final Logger logger = Logger.getLogger(Steps.class);

    public void initBrowser()
    {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe"); //TODO change driver path
        driver = new FirefoxDriver();
        //driver = new ChromeDriver();
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //driver.manage().window().maximize();
        logger.info("Browser started");
    }

    public void closeDriver()
    {
        driver.quit();
    }

    public void loginGithub(String username, String password)
    {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.openPage();
        loginPage.login(username, password);
    }

    public boolean createNewRepository(String repositoryName, String repositoryDescription)
    {
        MainPage mainPage = new MainPage(driver);
        mainPage.clickOnCreateNewRepositoryButton();
        CreateNewRepositoryPage createNewRepositoryPage = new CreateNewRepositoryPage(driver);
        String expectedRepoName = createNewRepositoryPage.createNewRepository(repositoryName, repositoryDescription);
        return expectedRepoName.equals(createNewRepositoryPage.getCurrentRepositoryName());
    }

    public boolean currentRepositoryIsEmpty()
    {
        CreateNewRepositoryPage createNewRepositoryPage = new CreateNewRepositoryPage(driver);
        return createNewRepositoryPage.isCurrentRepositoryEmpty();
    }

    public int searchRepoAndGetCount(String reponame) {
        SearchPage searchPage = new SearchPage(driver);
        searchPage.openPage();
        searchPage.searchRepo("linux");
        return searchPage.getFoundRepoCount();
    }

    public String createGistPrivate(String text) {
        GistPage gistPage = new GistPage(driver);
        gistPage.openPage();
        gistPage.createGistPrivate(text);
        return gistPage.getCreatedGistText();
    }

    public String createGistPublic(String text) {
        GistPage gistPage = new GistPage(driver);
        gistPage.openPage();
        gistPage.createGistPublic(text);
        return gistPage.getCreatedGistText();
    }

    public boolean updateProfileName() {
        SettingsPage settingsPage = new SettingsPage(driver);
        settingsPage.openPage();
        settingsPage.setEditTextName("Test Name = " + String.valueOf((new Random()).nextInt(9999)));
        settingsPage.clickUpdateProfile();
        return (!settingsPage.getLabelStatus().isEmpty());
    }

    public boolean updateProfilePassword() {
        SettingsAccountPage settingsAccountPage = new SettingsAccountPage(driver);
        settingsAccountPage.openPage();
        settingsAccountPage.setEditTextOldPassword("123456Aa");
        settingsAccountPage.setEditTextNewPassword("123456Aa");
        settingsAccountPage.setEditTextConfirmPassword("123456Aa");
        return (!settingsAccountPage.getLabelStatus().isEmpty());
    }

}