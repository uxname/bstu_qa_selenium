import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import steps.Steps;

public class SimpleTest {

    private Steps steps;
    private final String USERNAME = SettingsConsts.gitHubLogin;
    private final String PASSWORD = SettingsConsts.gitHubPassword;

    @BeforeMethod(description = "Init browser")
    public void setUp() {
        steps = new Steps();
        steps.initBrowser();
    }

    @Test
    public void tryCreateProject() {
        steps.loginGithub(USERNAME, PASSWORD);
        Assert.assertTrue(steps.createNewRepository("testRepo", "auto-generated test repo"));
        Assert.assertTrue(steps.currentRepositoryIsEmpty());
    }

    @Test
    public void trySearchRepo() {
        Assert.assertNotEquals(steps.searchRepoAndGetCount("linux"), 0);
    }

    @Test
    public void tryCreateGistPrivate() {
        String text = "just a text";
        Assert.assertEquals(steps.createGistPrivate(text), text);
    }

    @Test
    public void tryCreateGistPublic() {
        String text = "just a text";
        Assert.assertEquals(steps.createGistPublic(text), text);
    }

    @Test
    public void tryUpdateProfileName() {
        steps.loginGithub(USERNAME, PASSWORD);
        Assert.assertTrue(steps.updateProfileName());
    }

    @Test
    public void tryUpdateProfilePassword() {
        steps.loginGithub(USERNAME, PASSWORD);
        Assert.assertTrue(steps.updateProfilePassword());
    }


    @AfterMethod(description = "Stop Browser")
    public void stopBrowser() {
        steps.closeDriver();
    }

}
